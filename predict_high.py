from bigml.api import BigML
from bigml.deepnet import Deepnet
from time import strftime, gmtime


class Predict:
    api = None
    source = None
    deepnet = None
    origin_data = ''
    test_csv_path = ''
    predict_csv_path = ''

    def __init__(self, test_path, predict_path):
        # initialize api, deepnet, input data
        self.api = BigML('igorwaverider', '513dd22bc309e063bc365a65821d3d4c31d6dd1c')
        # self.deepnet = Deepnet('deepnet/5a32494759f5c3421b000567', self.api)
        self.deepnet = Deepnet('deepnet/5a2f9a8359f5c3421d0003e5', self.api)
        self.test_csv_path = test_path
        self.predict_csv_path = predict_path
        self.source = self.api.create_source(self.test_csv_path)
        with open(self.test_csv_path) as f:
            self.origin_data = f.read()
            f.close()

    def create_file(self):
        # get predict result
        predict = self.deepnet.predict(self.source)
        predict_value = predict['prediction']

        # create predict1.csv file
        with open(self.predict_csv_path, 'w') as f:
            cur_time = strftime("%Y-%m-%d %H:%M:%S", gmtime())
            new_data = cur_time + ", " + predict_value + " \n"
            f.write("Time, BinaryPivot \n")
            f.write(new_data)
            f.close()

    def predict(self):
        # get predict result
        predict = self.deepnet.predict(self.api.create_source(self.test_csv_path))
        predict_value = predict['prediction']

        # add new predict result to predict1.csv file
        with open(self.predict_csv_path, 'a') as f:
            cur_time = strftime("%Y-%m-%d %H:%M:%S", gmtime())
            new_data = cur_time + ", " + predict_value + " \n"
            f.write(new_data)
            f.close()

    def check_updated(self):
        is_updated = False
        with open(self.test_csv_path) as f:
            data = f.read()
            if data != self.origin_data:
                self.predict()
                self.origin_data = data
                is_updated = True
            f.close()
        return is_updated

    def run(self):
        self.check_updated()


if __name__ == '__main__':
    predict_instance = Predict('C:/high22/test1.csv', 'C:/high22/predict1.csv')
    predict_instance.create_file()
    while True:
        predict_instance.run()
